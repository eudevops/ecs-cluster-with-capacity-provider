# Terraform module to setup a ECS Cluster

This module sets up an ECS Cluster with Capacity Provider of EC2 instances using an Auto Scaling Group and Launch Template.

Don't reinvent the modules! This module is based on [Terraform Aws Modules](https://github.com/terraform-aws-modules) project that
does have a lot of community proved modules.

## Environment

### Install Terraform  
Check on how to install Terraform on your given OS at [this link](terraform.io/downloads.html)

### Pre-commit tools
This project contains a pre-commit file to execute checks at this source code. Check [Anton Babenko plugin](https://github.com/antonbabenko/pre-commit-terraform) to 
install the required dependencies.

## How to 

### Deploy this module on your terraform project

In your terraform project initialize the module like this:

```hcl
module "my-cluster" {
    source        = "git@gitlab.com:eudevops/ecs-cluster-with-capacity-provider.git?ref=main"
    instance_type = ""
    max_size      = ""
    min_size      = 1
    name          = ""
    tags          = ""
    vpc_id        = ""
    vpc_name      = ""
}
```

Fill the relevant configuration, note that some variables have a default value and you may not need to declare them above.


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.37 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.37 |
| <a name="provider_template"></a> [template](#provider\_template) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_asg"></a> [asg](#module\_asg) | terraform-aws-modules/autoscaling/aws | ~> 4.0 |
| <a name="module_ec2_profile"></a> [ec2\_profile](#module\_ec2\_profile) | terraform-aws-modules/ecs/aws//modules/ecs-instance-profile |  |
| <a name="module_ecs"></a> [ecs](#module\_ecs) | terraform-aws-modules/ecs/aws |  |

## Resources

| Name | Type |
|------|------|
| [aws_ecs_capacity_provider.prov1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_capacity_provider) | resource |
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ami.amazon_linux_ecs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_subnet_ids.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet_ids) | data source |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |
| [template_file.user_data](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_auto_scaling_name"></a> [auto\_scaling\_name](#input\_auto\_scaling\_name) | Name to use for auto scaling group and instances | `string` | `""` | no |
| <a name="input_capacity_provider_name"></a> [capacity\_provider\_name](#input\_capacity\_provider\_name) | Name to use for the capacity provider | `string` | `""` | no |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | Name to use for the ecs cluster | `string` | `""` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Default instance to be used on auto scaling group | `string` | n/a | yes |
| <a name="input_launch_template_name"></a> [launch\_template\_name](#input\_launch\_template\_name) | Name to use for asg launch template | `string` | `""` | no |
| <a name="input_max_size"></a> [max\_size](#input\_max\_size) | Maximum number of running instances | `number` | n/a | yes |
| <a name="input_min_size"></a> [min\_size](#input\_min\_size) | Minimum number of running instances | `number` | `1` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to apply to the cluster | `string` | `null` | no |
| <a name="input_security_group_name"></a> [security\_group\_name](#input\_security\_group\_name) | Name to use for the security group | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to apply on resources | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC id to deploy resources | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->