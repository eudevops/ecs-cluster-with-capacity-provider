variable "instance_type" {
  description = "Default instance to be used on auto scaling group"
  type        = string
}

variable "max_size" {
  description = "Maximum number of running instances"
  type        = number
}

variable "min_size" {
  description = "Minimum number of running instances"
  type        = number
  default     = 1
}

variable "name" {
  description = "Name to apply to the cluster"
  type        = string
  default     = null
}

variable "tags" {
  description = "Tags to apply on resources"
  type        = map(string)
  default     = {}
}

variable "vpc_id" {
  description = "VPC id to deploy resources"
  type        = string
}


## Fill these if you want to use terraform import or that you dont want use the default names

variable "auto_scaling_name" {
  description = "Name to use for auto scaling group and instances"
  type        = string
  default     = ""
}

variable "capacity_provider_name" {
  description = "Name to use for the capacity provider"
  type        = string
  default     = ""
}

variable "cluster_name" {
  description = "Name to use for the ecs cluster"
  type        = string
  default     = ""
}

variable "launch_template_name" {
  description = "Name to use for asg launch template"
  type        = string
  default     = ""
}

variable "security_group_name" {
  description = "Name to use for the security group"
  type        = string
  default     = ""
}
